## 技术选型

| 技术           | 说明     |
|--------------|--------|
| spring boot  | 2.6.11 |
| mybatis      | 2.2.2  |
| mybatis-plus | 3.5.2  |


## 构建docker镜像

```shell
docker build -f DockerFileJdk -t model-web:1.1.0 .
docker build -f DockerFileCentos7 -t model-web:1.2.0 .
```

## 运行镜像

```shell
# 使用内部配置文件
docker run -p 51155:51155 -p 51156:51156 -p 51157:51157 --name web --restart=always -d model-web:1.1.0

# 指定自定义配置文件
docker run -p 51155:51155 \
-p 51156:51156 \
-p 51157:51157 \
--name web \
--restart=always \
-v /e/code/idea/spring-boot-model/spring-boot-model-web/dockerFileDemo/config:/data/web/config \
-d  model-web:1.2.0

# 1.1.0
docker run -p 51155:51155 -p 51156:51156 -p 51157:51157 --name web --restart=always -v E:\code\idea\spring-boot-model\dockerFileDemo\config:/data/web/config -d  model-web:1.1.0

# 1.2.0
docker run -p 51155:51155 -p 51156:51156 -p 51157:51157 --name web --restart=always -v E:\code\idea\spring-boot-model\dockerFileDemo\config:/data/web/config -d  model-web:1.2.0

```

## git

```bash
git init

git add -A
git rm jdk-17.0.3_linux-x64_bin.tar.gz

git commit -m "init"
git remote add origin https://gitee.com/cps007/spring-boot-model.git
git push -u origin "master"

```
