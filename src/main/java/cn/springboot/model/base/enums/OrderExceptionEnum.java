package cn.springboot.model.base.enums;

/**
 * @author jf
 * @description 订单异常枚举
 * @Date 2021/7/21 20
 */
public enum OrderExceptionEnum {
    /**
     * 成功
     */
    SUCCESS(200),
    /**
     * 警告
     */
    WARN(301),
    PRODUCT_UP_EXCEPTION(11000, "商品上架异常"),
    NO_STOCK_EXCEPTION(21000, "商品库存不足");

    private int code;
    private String message;

    OrderExceptionEnum() {
    }

    OrderExceptionEnum(int code) {
        this.code = code;
    }

    OrderExceptionEnum(String message) {
        this.message = message;
    }

    OrderExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getmessage() {
        return message;
    }
}
