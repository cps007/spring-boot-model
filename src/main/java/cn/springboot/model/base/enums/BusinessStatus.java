package cn.springboot.model.base.enums;

/**
 * 操作状态
 *
 * @author jf
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
