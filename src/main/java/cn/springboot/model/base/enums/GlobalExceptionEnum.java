package cn.springboot.model.base.enums;

/**
 * @author jf
 * @description 错误码和错误信息定义类
 * 1. 错误码定义规则为5为数字
 * 2. 前两位表示业务场景，最后三位表示错误码。例如：100001。10:通用 001:系统未知异常
 * 3. 维护错误码后需要维护错误描述，将他们定义为枚举形式
 * @date 2021/7/21 20
 */
public enum GlobalExceptionEnum {
    /**
     * 成功
     */
    SUCCESS(200),
    /**
     * 警告
     */
    WARN(301),
    /**
     * 错误
     */
    TOKEN_INVALID(401, "token失效，请重新登录"),
    ERROR_404(404, "路径不存在，请检查路径是否正确"),
    /**
     * 服务器
     */
    ERROR(500),
    ERROR_SHIRO_PERMISSIONS(500, "没有权限，请联系管理员授权"),
    ERROR_DB_ISDATA(500, "数据库中已存在该记录"),
    ERROR_DB_EXCEPTION(500, "数据库异常"),
    ACCOUNT_LOCKED(500, "账号已被锁定，请联系管理员"),
    ACCOUNT_NULL(500, "账号不存在，请联系管理员"),
    DEMO_MODE(500, "演示模式，不允许操作"),
    MALICE_REPEAT_SUBMIT(500, "经系统检测，判定为恶意请求"),
    REPEAT_SUBMIT(500, "请勿重复请求"),
    USER_NOT_LOG(501, "账号还没有登录，请登陆"),
    /**
     * 系统
     */
    UNKNOWN_EXCEPTION(10000, "系统未知异常，请联系管理员。"),
    VALID_EXCEPTION(10010, "参数格式校验失败"),
    VALID_JSR_EXCEPTION(10011, "违反 JSR 约束异常"),
    BIND_EXCEPTION(10012, "数据校验-绑定异常"),
    TO_MANY_REQUEST(10020, "请求流量过大，请稍后再试"),
    SMS_CODE_EXCEPTION(10030, "验证码获取频率太高，请稍后再试"),
    NOT_SHIRO_EXCEPTION(10040, "没有权限，请联系管理员授权"),
    /**
     * 用户
     */
    USER_EXIST_EXCEPTION(11001, "存在相同的用户"),
    PHONE_EXIST_EXCEPTION(11002, "存在相同的手机号"),
    LOGINACCT_PASSWORD_EXCEPTION(11003, "账号或密码错误"),
    /**
     * 资源请求
     */
    PATH_EXCEPTION(40004, "路径不存在，请检查路径是否正确"),
    DEMO_EXCEPTION(49999, "演示模式");

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    GlobalExceptionEnum() {
    }

    GlobalExceptionEnum(int code) {
        this.code = code;
    }

    GlobalExceptionEnum(String message) {
        this.message = message;
    }

    GlobalExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
