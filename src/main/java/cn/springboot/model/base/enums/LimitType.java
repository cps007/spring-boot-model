package cn.springboot.model.base.enums;

/**
 * 限流类型
 *
 * @author jf
 * @version 1.0
 * @Description 描述
 * @date 2022/05/29 11:30
 */
public enum LimitType {
    /**
     * 默认策略全局限流
     */
    DEFAULT,

    /**
     * 根据请求者IP进行限流
     */
    IP
}
