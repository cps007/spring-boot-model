package cn.springboot.model.base.common;

import cn.springboot.model.base.enums.GlobalExceptionEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 * @author jf
 */
public class R extends HashMap<String, Object> {

    public R() {
        put("code", 200);
        put("message", "success");
    }

    public static R error() {
        return error(GlobalExceptionEnum.UNKNOWN_EXCEPTION.getCode(), GlobalExceptionEnum.UNKNOWN_EXCEPTION.getMessage());
    }

    public static R error(String message) {
        return error(GlobalExceptionEnum.ERROR.getCode(), message);
    }

    public static R error(int code, String message) {
        R r = new R();
        r.put("code", code);
        r.put("message", message);
        return r;
    }

    public static R ok(String message) {
        R r = new R();
        r.put("message", message);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }

    @Override
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public Integer getCode() {

        return (Integer) this.get("code");
    }

}
