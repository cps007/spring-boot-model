package cn.springboot.model.base.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解防止表单重复提交
 *
 * @author jf
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Submit {
    /**
     * 设置请求次数上限，在于 triggerTime() 内
     *
     * @return
     */
    int lockCount() default 4;

    /**
     * 锁定请求时间<p/>
     * 单位:分钟
     *
     * @return
     */
    long lockTime() default 60;

    /**
     * 触发重复提交时间
     *
     * @return
     */
    long triggerTime() default 6;

}
