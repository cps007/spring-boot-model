package cn.springboot.model.base.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author jf
 * @version 1.0
 * @Description 描述
 * @date 2022/06/02 20:21
 */
@Configuration
public class RedisConfig {

    /**
     * redis:V -> json序列化配置<br/>
     * ----> JDK 序列化方式 （默认） JdkSerializationRedisSerializer<br/>
     * ----> String 序列化方式 StringRedisSerializer<br/>
     * ----> JSON 序列化方式 Jackson2JsonRedisSerializer、GenericJackson2JsonRedisSerializer<br/>
     * ----> XML 序列化方式 OxmSerializer<br/>
     */
    @Bean(name = "redisTemplate")
    @SuppressWarnings("all")
    public <T> RedisTemplate<Object, T> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<Object, T> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);

//        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
//        jackson2JsonRedisSerializer.setObjectMapper(mapper);
////////////////////////////////////////////////////////////////
//        GenericJackson2JsonRedisSerializer genericJackson2JsonRedisSerializer = new GenericJackson2JsonRedisSerializer();
        FastJson2JsonRedisSerializer fastJson2JsonRedisSerializer = new FastJson2JsonRedisSerializer(Object.class);

        // redis:K 的序列化
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        //key采用String的序列方式
        redisTemplate.setKeySerializer(stringRedisSerializer);
        //hash的key采用String的序列方式
        redisTemplate.setHashKeySerializer(stringRedisSerializer);

        //value的序列化方式采用Jackson
        redisTemplate.setValueSerializer(fastJson2JsonRedisSerializer);
        //hash的value采用jackson
        redisTemplate.setHashValueSerializer(fastJson2JsonRedisSerializer);

        redisTemplate.afterPropertiesSet();

        return redisTemplate;
    }

    @Bean //(name = "stringRedisTemplate")
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
        return new StringRedisTemplate(redisConnectionFactory);
    }

}
