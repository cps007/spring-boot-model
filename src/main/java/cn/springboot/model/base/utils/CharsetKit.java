package cn.springboot.model.base.utils;

/**
 * @author jf
 * @version 1.0
 * @Description 描述
 * @date 2022/06/02 13:18
 */
public class CharsetKit {
    public static final String UTF_8 = "UTF-8";
    public static final String GBK = "GBK";
    public static final String ISO_8859_1 = "ISO-8859-1";
    public static final String US_ASCII = "US-ASCII";
}
