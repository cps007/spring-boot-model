package cn.springboot.model.base.utils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期处理
 *
 * @author jf
 * @version 1.0
 * @date 2022/05/20 12:12
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {

    /**
     * 时间格式(yyyy-MM-dd)
     */
    public final static String DATE_PATTERN_ONE = "yyyy-MM-dd";
    public final static String DATE_PATTERN_TWO = "yyyyMMdd";
    /**
     * 时间格式(yyyy-MM-dd HH:mm:ss)
     */
    public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 获取当前月
     */
    private static Calendar calendar = Calendar.getInstance();
    public static int amonth = calendar.get(Calendar.MONTH) + 1;//获取到类型1-12

/////////////////////////////获取////////////////////////////////

    /**
     * 当前系统时间
     */
    public static long getDataTimeByLong() {
        return LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    /**
     * 获取LocalDateTime的指定日期格式
     *
     * @param ofPattern 设置返回时间格式：yyyy-MM-dd HH:mm:ss
     * @return 2018-11-27 10:41:47
     */
    public static String dateFormat(String ofPattern) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(ofPattern));
    }

//////////////////////////////////////转换//////////////////////////////////

    /**
     * 将 Tue Jun 30 12:46:10 CST 2020 或 long转为yyyy-MM-dd HH:mm:ss
     *
     * @param tarDate 时间 (Tue Jun 30 12:46:10 CST 2020 或1595660977000)
     * @return
     */
    public static String dateFormatByObject(Object tarDate) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_PATTERN);
        return formatter.format(tarDate);
    }

    /**
     * 将Strng转long
     *
     * @param ofPattern [时间格式：yyyy-MM-dd HH:mm:ss] 例如:2021-07-07 23:59:384
     * @return 1925465465345 long
     */
    public static Long dateFormatStringByLong(String ofPattern) {
        String time = dateFormat(ofPattern);
        LocalDateTime da = LocalDateTime.parse(time, DateTimeFormatter.ofPattern(ofPattern));
        return da.toInstant(ZoneOffset.of("+8")).toEpochMilli();
    }

    /**
     * 将字符串格式转日期，如：yyyy-MM-dd ||自定义格式
     *
     * @param date       日期字符串
     * @param dateFormat 设置将字符串格式转日期格式，这个与date的格式必须一致
     * @param tarFormat  设置目标格式
     * @return 返回格式化的日期，默认格式：yyyy-MM-dd
     */
    public static String strToDateFormat(String date, String dateFormat, String tarFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(StringUtils.isBlank(dateFormat) ? DATE_PATTERN_TWO : dateFormat);
        try {
            formatter.setLenient(false);
            Date newDate = formatter.parse(date);
            formatter = new SimpleDateFormat(StringUtils.isBlank(tarFormat) ? DATE_PATTERN_ONE : tarFormat);
            return formatter.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error";
    }

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     *
     * @param date 日期
     * @return 返回yyyy-MM-dd格式日期
     */
    public static String format(Date date) {
        return format(date, DATE_PATTERN_ONE);
    }

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     *
     * @param date    日期
     * @param pattern 格式，如：DateUtils.DATE_TIME_PATTERN
     * @return 返回yyyy-MM-dd格式日期
     */
    public static String format(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        }
        return null;
    }
//////////////////运算//////////////////////

    /**
     * 对日期的【秒】进行加/减
     *
     * @param dateTime 日期
     * @param seconds  秒数，负数为减
     * @return 加/减几秒后的日期
     */
    public static DateTime addDateTimeSeconds(DateTime dateTime, int seconds) {
        return new DateTime(dateTime).plusSeconds(seconds).toDateTime();
    }

    public static Date addDateSeconds(Date date, int seconds) {
        return new DateTime(date).plusSeconds(seconds).toDate();
    }

    /**
     * 对日期的【分钟】进行加/减
     *
     * @param dateTime 日期
     * @param minutes  分钟数，负数为减
     * @return 加/减几分钟后的日期
     */
    public static DateTime addDateTimeMinutes(DateTime dateTime, int minutes) {
        DateTime nowDateTime = new DateTime(dateTime);
        return nowDateTime.plusMinutes(minutes).toDateTime();
    }

    public static Date addDateMinutes(Date date, int minutes) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusMinutes(minutes).toDate();
    }

    /**
     * 对日期的【小时】进行加/减
     *
     * @param dateTime 日期
     * @param hours    小时数，负数为减
     * @return 加/减几小时后的日期
     */
    public static DateTime addDateTimeHours(DateTime dateTime, int hours) {
        return new DateTime(dateTime).plusHours(hours).toDateTime();
    }

    public static Date addDateHours(Date date, int hours) {
        return new DateTime(date).plusHours(hours).toDate();
    }


    /**
     * 对日期的【天】进行加/减
     *
     * @param dateTime 日期
     * @param days     天数，负数为减
     * @return 加/减几天后的日期
     */
    public static DateTime addDateTimeDays(DateTime dateTime, int days) {
        return new DateTime(dateTime).plusDays(days).toDateTime();
    }

    public static Date addDateDays(Date date, int days) {
        return new DateTime(date).plusDays(days).toDate();
    }

    /**
     * 对日期的【周】进行加/减
     *
     * @param dateTime 日期
     * @param weeks    周数，负数为减
     * @return 加/减几周后的日期
     */
    public static DateTime addDateTimeWeeks(DateTime dateTime, int weeks) {
        return new DateTime(dateTime).plusWeeks(weeks).toDateTime();
    }

    public static Date addDateWeeks(Date date, int weeks) {
        return new DateTime(date).plusWeeks(weeks).toDate();
    }

    /**
     * 对日期的【月】进行加/减
     *
     * @param dateTime 日期
     * @param months   月数，负数为减
     * @return 加/减几月后的日期
     */
    public static DateTime addDateTimeMonths(DateTime dateTime, int months) {
        return new DateTime(dateTime).plusMonths(months).toDateTime();
    }

    public static Date addDateMonths(Date date, int months) {
        return new DateTime(date).plusMonths(months).toDate();
    }

    /**
     * 对日期的【年】进行加/减
     *
     * @param dateTime 日期
     * @param years    年数，负数为减
     * @return 加/减几年后的日期
     */
    public static DateTime addDateTimeYears(DateTime dateTime, int years) {
        return new DateTime(dateTime).plusYears(years).toDateTime();
    }

    public static Date addDateYears(Date date, int years) {
        return new DateTime(date).plusYears(years).toDate();
    }

}
