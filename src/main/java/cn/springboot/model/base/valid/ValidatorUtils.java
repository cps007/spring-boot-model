package cn.springboot.model.base.valid;

import cn.springboot.model.base.exception.GlobalException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Objects;
import java.util.Set;

/**
 * hibernate-validator校验工具类
 * <p>
 * 参考文档：http://docs.jboss.org/hibernate/validator/5.4/reference/en-US/html_single/
 *
 * @author jf
 * @version 1.0
 * @date 2022/05/20 12:12
 */
public class ValidatorUtils {
    //    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     *
     * @param object 待校验对象
     * @param groups 待校验的组
     * @throws GlobalException 校验不通过，则报RRException异常
     */
    public static void validateEntity(Object object, Class<?>... groups) throws GlobalException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!Objects.isNull(constraintViolations)) {
            StringBuilder message = new StringBuilder();
            for (ConstraintViolation<Object> constraint : constraintViolations) {
                message.append(constraint.getMessage()).append("<br>");
            }
            throw new GlobalException(message.toString());
        }
    }
}
