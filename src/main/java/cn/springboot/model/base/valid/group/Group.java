package cn.springboot.model.base.valid.group;

import javax.validation.GroupSequence;

/**
 * 默认分组声明：添加、更新
 * 定义校验顺序，如果AddGroup组失败，则UpdateGroup组不会再校验
 *
 * @author jf
 */
@GroupSequence({AddGroup.class, UpdateGroup.class})
public interface Group {

}
