package cn.springboot.model.base.valid;

import cn.springboot.model.base.exception.UtilException;
import org.apache.commons.lang3.StringUtils;

/**
 * 数据校验
 *
 * @author jf
 * @version 1.0
 * @date 2022/05/20 12:12
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new UtilException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new UtilException(message);
        }
    }
}
