package cn.springboot.model.base.exception;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * @author jf
 * @Description 演示模式异常
 * @Date 2021/7/21 20
 */
@Getter
@Setter
public class DemoModeException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;

    private String message;
    private int code = 500;

    public DemoModeException() {
        super();
    }

    public DemoModeException(String message) {
        super(message);
        this.message = message;
    }

    public DemoModeException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
