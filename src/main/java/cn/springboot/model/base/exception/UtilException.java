package cn.springboot.model.base.exception;

import java.io.Serial;

/**
 * 工具类异常
 *
 * @author jf
 * @version 1.0
 * @date 2022/5/27 22:46
 */
public class UtilException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 8247610319171014183L;

    UtilException() {
        super();
    }

    public UtilException(Throwable e) {
        super(e.getMessage(), e);
    }

    public UtilException(String message) {
        super(message);
    }

    public UtilException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
