package cn.springboot.model.base.exception;

import cn.springboot.model.base.common.R;
import cn.springboot.model.base.enums.GlobalExceptionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author jf
 * @Description 全局异常处理器
 * @Date 2021/7/21 20
 */
@RestControllerAdvice
public class GlobalExceptionControllerAdvice {
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionControllerAdvice.class);

    /**
     * 未知异常
     *
     * @param exception
     * @return R
     */
    @ExceptionHandler(Exception.class)
    public R handleException(Exception exception) {
        log.error("未知异常，请联系管理员.{} {}", exception.getMessage(), exception);
        return R.error(GlobalExceptionEnum.UNKNOWN_EXCEPTION.getCode(), GlobalExceptionEnum.UNKNOWN_EXCEPTION.getMessage());
    }

    /**
     * 处理自定义异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(GlobalException.class)
    public R handleException(GlobalException exception) {
        log.error("自定义异常 {}", exception.getMessage());
        return new R().put("code", exception.getCode()).put("message", exception.getMessage());
    }

    /**
     * 数据库异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(value = {DBException.class})
    public R handleDBException(DBException exception) {
        log.error("数据库操作异常 {}", exception.getMessage());
        return R.error(GlobalExceptionEnum.ERROR_DB_EXCEPTION.getCode(), GlobalExceptionEnum.ERROR_DB_EXCEPTION.getMessage());
    }

    /**
     * 数据库主键重复异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(value = {DuplicateKeyException.class})
    public R handleDuplicateKeyException(DuplicateKeyException exception) {
        log.error("数据库重复主键异常 {}", exception.getMessage());
        return R.error(GlobalExceptionEnum.ERROR_DB_ISDATA.getCode(), GlobalExceptionEnum.ERROR_DB_ISDATA.getMessage());
    }


    /**
     * 数据校验-方法参数无效异常
     * 当对带有@Valid注释的参数的验证失败时抛出异常。
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleVaildException(MethodArgumentNotValidException exception) {
        log.error("方法参数验证失败 {}，异常类型：{}", exception.getMessage(), exception.getClass());
        BindingResult bindingResult = exception.getBindingResult();
        Map<String, String> errorMap = new HashMap<>(20);
        bindingResult.getFieldErrors().forEach((fieldError) -> {
            errorMap.put("字段:[" + fieldError.getField() + "，值[" + fieldError.getRejectedValue() + "]", fieldError.getDefaultMessage());
        });
        return R.error(GlobalExceptionEnum.VALID_EXCEPTION.getCode(), GlobalExceptionEnum.VALID_EXCEPTION.getMessage()).put("data", errorMap);
    }

    /**
     * 数据校验-绑定异常
     *
     * @param bindException
     * @return
     */
    @ExceptionHandler(BindException.class)
    public R validatedBindException(BindException bindException) {
        log.error("数据校验-绑定异常 {}，异常类型：{}", bindException.getMessage(), bindException.getClass());
        Map<String, String> messageMap = new HashMap<>(20);
        // 拿到@NotNull,@NotBlank和 @NotEmpty等注解上的message值
        bindException.getFieldErrors().forEach((fieldError) -> {
            messageMap.put("字段:[" + fieldError.getField() + "，值{" + fieldError.getRejectedValue() + "}]", fieldError.getDefaultMessage());
        });
        return R.error(GlobalExceptionEnum.BIND_EXCEPTION.getCode(), GlobalExceptionEnum.BIND_EXCEPTION.getMessage()).put("data", messageMap);
    }

    /**
     * JSR 规范中的验证异常，嵌套检验问题
     * 报告违反约束的结果。
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public R constraintViolationException(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> violations = ex.getConstraintViolations();
        String message = violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(";"));
        return R.error(GlobalExceptionEnum.VALID_JSR_EXCEPTION.getCode(), GlobalExceptionEnum.VALID_JSR_EXCEPTION.getMessage()).put("data", message);
    }


    /**
     * 路径异常
     *
     * @param exception
     * @return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public R handlerNoFoundException(NoHandlerFoundException exception) {
        log.error(exception.getMessage());
        return R.error(GlobalExceptionEnum.ERROR_404.getCode(), GlobalExceptionEnum.ERROR_404.getMessage());
    }


    /**
     * 没有权限异常
     *
     * @param exception
     * @return
     */
//    @ExceptionHandler(AuthorizationException.class)
//    public R handleAuthorizationException(AuthorizationException exception) {
//         log.error(exception.getMessage());
//        return R.error(GlobalExceptionEnum.ERROR_SHIRO_PERMISSIONS.getCode(), GlobalExceptionEnum.ERROR_SHIRO_PERMISSIONS.getmessage());
//    }

    /**
     * 演示模式异常
     */
    @ExceptionHandler(DemoModeException.class)
    public R demoModeException(DemoModeException exception) {
        return R.error(GlobalExceptionEnum.DEMO_MODE.getCode(), GlobalExceptionEnum.DEMO_MODE.getMessage());
    }

}
