package cn.springboot.model.base.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.DuplicateKeyException;

import java.io.Serial;

/**
 * @author jf
 * @Description 数据库异常
 * @Date 2021/7/21 20
 */
@Getter
@Setter
public class DBException extends DuplicateKeyException {
    @Serial
    private static final long serialVersionUID = 1L;

    private String message;
    private int code = 500;

    public DBException(String message) {
        super(message);
        this.message = message;
    }

    public DBException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public DBException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }
}
