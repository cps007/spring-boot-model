package cn.springboot.model.base.exception;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * 受检查异常
 *
 * @author jf
 * @version 1.0
 * @date 2022/5/27 21:55
 */
@Getter
@Setter
public class CheckedException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;

    public CheckedException(String message) {
        super(message);
    }

    public CheckedException(Throwable cause) {
        super(cause);
    }

    public CheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
