package cn.springboot.model.base.exception;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * @author jf
 * @Description 全局异常
 * @Date 2021/7/21 20
 */
@Getter
@Setter
public class GlobalException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;

    private String message;
    private int code = 500;

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    GlobalException() {
        super();
    }

    public GlobalException(String message) {
        super(message);
        this.message = message;
    }

    public GlobalException(int code, String message) {
        super(message);
        this.message = message;
        this.code = code;
    }

    public GlobalException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    public GlobalException(int code, String message, Throwable e) {
        super(message, e);
        this.message = message;
        this.code = code;
    }
}
