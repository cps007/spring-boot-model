package cn.springbooot.model.controller;

import cn.springboot.model.base.annotation.Log;
import cn.springboot.model.base.annotation.RequiresPermissions;
import cn.springboot.model.base.annotation.Submit;
import cn.springboot.model.base.common.R;
import cn.springboot.model.base.enums.BusinessType;
import cn.springboot.model.service.DemoClassService;
import cn.springboot.model.service.param.QueryPageParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author jf
 * @description 班级控制器
 * @since 2022-05-31 21:44:29
 */
@Api(value = "班级控制器", tags = "班级控制器")
@RestController
@RequestMapping("/class")
public class DemoClassController {

    @Autowired
    private DemoClassService demoClassService;

    @Log(title = "添加班级", businessType = BusinessType.INSERT)
    @ApiOperation(value = "添加班级")
    @Submit
    @PostMapping("/add")
    public R addStu() {
        return demoClassService.addClas();
    }

    @Log(title = "删除班级", businessType = BusinessType.INSERT)
    @ApiOperation(value = "删除班级")
    @Submit
    @DeleteMapping("/dele")
    public R deleStu() {
        return demoClassService.deleClas();
    }

    @Submit
    @ApiOperation(value = "获取班级list")
    @PostMapping("/getClass")
    public R getClas(@RequestBody QueryPageParam queryPage) {
        return demoClassService.getClas(queryPage);
    }

    @Submit
    @ApiOperation(value = "获取班级 by ID")
    @ApiImplicitParam(name = "id", value = "班级id", paramType = "query", required = true, dataType = "Integer", dataTypeClass = Integer.class)
    @GetMapping("/getClassById")
    public R getClassById(@RequestParam Integer id) {
        return demoClassService.getClassById(id);
    }
}