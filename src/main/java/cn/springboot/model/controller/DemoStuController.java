package cn.springbooot.model.controller;

import cn.springboot.model.base.annotation.Log;
import cn.springboot.model.base.annotation.Submit;
import cn.springboot.model.base.common.R;
import cn.springboot.model.base.enums.BusinessType;
import cn.springboot.model.service.DemoStuService;
import cn.springboot.model.service.param.QueryPageParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author jf
 * @description 学生测试
 * @since 2022-05-31 21:44:29
 */
@Api(value = "学生控制器", tags = "学生控制器")
@RestController
@RequestMapping("/stu")
public class DemoStuController {

    @Autowired
    private DemoStuService demoStuService;

    /**
     * @return
     */
    @Log(title = "添加学生", businessType = BusinessType.INSERT)
    @ApiOperation(value = "添加学生")
    @Submit
    @PostMapping(value = "/add")
    public R addStu() {
        return demoStuService.addStu();
    }

    /**
     * @return
     */
    @Log(title = "删除学生", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除学生")
    @Submit
    @DeleteMapping("/dele")
    public R deleStu() {
        return demoStuService.deleStu();
    }

    /**
     * @param queryPage
     * @return
     */
    @ApiOperation(value = "获取学生list")
    @Submit
    @PostMapping("/getStu")
    public R getStu(@RequestBody QueryPageParam queryPage) {
        return demoStuService.getStu(queryPage);
    }

    /**
     * @param id
     * @return
     */
    @Submit
    @ApiOperation(value = "获取学生by ID")
    @ApiImplicitParam(name = "id", value = "学生id", paramType = "query", required = true, dataType = "Integer", dataTypeClass = Integer.class)
    @GetMapping("/getStuById")
    public R getStuById(@RequestParam Integer id) {
        return demoStuService.getStuById(id);
    }
}