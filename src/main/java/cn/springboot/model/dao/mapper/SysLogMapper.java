package cn.springboot.model.dao.mapper;

import cn.springboot.model.dao.entity.SysLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author jf
 * @description 针对表【sys_log(操作日志记录)】的数据库操作Mapper
 * @createDate 2022-06-02 12:43:04
 * @Version 1.0
 */
public interface SysLogMapper extends BaseMapper<SysLogEntity> {

}