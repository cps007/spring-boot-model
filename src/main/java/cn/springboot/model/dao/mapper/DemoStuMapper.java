package cn.springboot.model.dao.mapper;

import cn.springboot.model.dao.entity.DemoStuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author jf
 * @Description describe for you
 * @Date 2020/8/31 15:44
 * @Version 1.0
 */
public interface DemoStuMapper extends BaseMapper<DemoStuEntity> {

}