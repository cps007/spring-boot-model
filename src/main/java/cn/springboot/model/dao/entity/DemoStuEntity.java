package cn.springboot.model.dao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

import static com.baomidou.mybatisplus.annotation.IdType.INPUT;

/**
 * (demo_stu)实体类
 *
 * @author jf
 * @description
 * @since 2022-05-31 21:44:29
 */
@Data
@Accessors(chain = true)
@TableName("demo_stu")
@ApiModel(value = "学生实体类", description = "学生实体类")
public class DemoStuEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = INPUT)
    @ApiModelProperty(value = "id", required = true)
    private Long id;
    /**
     * 学生名称
     */
    @ApiModelProperty(value = "学生名称", required = true)
    private String stuName;
    /**
     * 学生年龄
     */
    @ApiModelProperty(value = "学生年龄", required = true)
    private String stuAge;
    /**
     * 班级编号
     */
    @ApiModelProperty(value = "班级编号", required = true)
    private Long classId;

}