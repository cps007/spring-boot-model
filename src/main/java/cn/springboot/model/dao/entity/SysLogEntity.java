package cn.springboot.model.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作日志记录
 *
 * @author jf
 * @version 1.0
 * @date 2022/5/26 20:50
 */

@Data
@Accessors(chain = true)
@TableName("sys_log")
@ApiModel(value = "操作日志记录", description = "操作日志记录")
public class SysLogEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 日志主键
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "日志主键", required = true)
    private Long id;

    /**
     * 用户
     */
    @ApiModelProperty(value = "用户", required = true)
    private String userName;

    /**
     * 操作详情
     */
    @ApiModelProperty(value = "操作详情", required = true)
    private String operation;

    /**
     * 请求URL
     */
    @ApiModelProperty(value = "请求URL", required = true)
    private String url;

    /**
     * 请求方法
     */
    @ApiModelProperty(value = "请求方法", required = true)
    private String method;

    /**
     * 请求方式
     */
    @ApiModelProperty(value = "请求方式", required = true)
    private String requestMethod;

    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数", required = true)
    private String params;

    /**
     * 执行耗时
     */
    @ApiModelProperty(value = "执行耗时", required = true)
    private Long runTime;

    /**
     * 用户ip
     */
    @ApiModelProperty(value = "用户ip", required = true)
    private String ip;
    /**
     * 用户省市名称
     */
    @ApiModelProperty(value = "用户省市名称", required = true)
    private String location;

    /**
     * 模块标题
     */
    @ApiModelProperty(value = "模块标题", required = true)
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    @ApiModelProperty(value = "业务类型（0其它 1新增 2修改 3删除）", required = true)
    private Integer businessType;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @ApiModelProperty(value = "操作类别", required = true)
    private Integer type;

    /**
     * 返回参数
     */
    @ApiModelProperty(value = "返回参数", required = true)
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @ApiModelProperty(value = "操作状态", required = true)
    private Integer status;

    /**
     * 错误消息
     */
    @ApiModelProperty(value = "错误消息", required = true)
    private String errorMsg;

    /**
     * 用户请求操作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "用户请求操作时间", required = true)
    private LocalDateTime createTime;
}