package cn.springboot.model.dao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

import static com.baomidou.mybatisplus.annotation.IdType.INPUT;

/**
 * (demo_class)实体类
 *
 * @author jf
 * @description
 * @since 2022-05-31 21:44:29
 */
@Data
@Accessors(chain = true)
@TableName("demo_class")
@ApiModel(value = "班级实体类", description = "班级实体类")
public class DemoClassEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = INPUT)
    @ApiModelProperty(value = "id", required = true)
    private Long id;
    /**
     * 班级名称
     */
    @ApiModelProperty(value = "班级名称", required = true)
    private String className;
    /**
     * 班级编号
     */
    @ApiModelProperty(value = "班级编号", required = true)
    private String classMn;

}