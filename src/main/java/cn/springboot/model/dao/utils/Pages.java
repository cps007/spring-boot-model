package cn.springboot.model.dao.utils;

import cn.springboot.model.base.utils.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * mybatisplus-分页工具类
 *
 * @author jf
 * @version 1.0
 * @date 2022/05/20 12:12
 */
@Data
public class Pages implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 列表数据
     */
    private List<?> data = Collections.emptyList();

    /**
     * 数据总条数
     */
    private long total = 0;
    /**
     * 每页记录数
     */
    private long size = 10;
    /**
     * 当前页数
     */
    private long current = 1;
    /**
     * 分页总页数
     */
    private long pages = 0;

    public Pages() {

    }

    /**
     * @param data    数据
     * @param size    每页记录数
     * @param current 当前页数
     */
    public Pages(final List<?> data, final long size, final long current) {
        this.data = data;
        this.total = StringUtils.isNotEmpty(data) ? data.size() : 0;
        this.size = size;
        this.current = current;
        this.pages = total >= size ? (long) Math.ceil((double) total / size) : 0;
        if (total >= size && total % size != 0) {
            pages++;
        }
    }

    /**
     * 统一封装 IPage
     *
     * @param page
     */
    public Pages(final IPage<?> page) {
        if (page.getRecords().size() > 0 && page.getTotal() > 0) {
            this.data = page.getRecords();
            this.total = page.getTotal();
            this.size = page.getSize();
            this.current = page.getCurrent();
            this.pages = page.getPages();
        }
    }
}
