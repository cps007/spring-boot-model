package cn.springboot.model.dao.utils;

import cn.springboot.model.base.constant.Constants;
import cn.springboot.model.base.xss.SQLFilter;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 查询分页参数
 *
 * @author jf
 * @version 1.0
 * @date 2022/05/20 12:12
 */
@Data
public class QueryPage<T> {

    /**
     * 当前页
     */
    protected long current = 1;

    /**
     * 每页显示条数，默认 15
     */
    protected long limit = 10;


    public QueryPage() {
    }

    /**
     * 默认查询参数 current:1，limit:10
     *
     * @param current 当前页面
     * @param limit   每页查询几条
     * @return
     */
    public IPage<T> getPage(long current, long limit) {
        Map<String, Object> map = new HashMap<>();
        map.put("current", current);
        map.put("limit", limit);
        return getPage(map, null, false);
    }

    private IPage<T> getPage(Map<String, Object> params, String defaultOrderField, boolean isAsc) {
        if (params.get(Constants.CUR_PAGE) != null) {
            this.current = (long) params.get(Constants.CUR_PAGE);
        }
        if (params.get(Constants.LIMIT) != null) {
            this.limit = (long) params.get(Constants.LIMIT);
        }

        //分页对象
        Page<T> page = new Page<T>(this.current, this.limit);

        //分页参数
        params.put(Constants.PAGE, page);

        //排序字段
        //防止SQL注入（因为sidx、order是通过拼接SQL实现排序的，会有SQL注入风险）
        String orderField = SQLFilter.sqlInject((String) params.get(Constants.ORDER_FIELD));
        String order = (String) params.get(Constants.ORDER);


        //前端字段排序
        if (StringUtils.isNotEmpty(orderField) && StringUtils.isNotEmpty(order)) {
            if (Constants.ASC.equalsIgnoreCase(order)) {
                return page.addOrder(OrderItem.asc(orderField));
            } else {
                return page.addOrder(OrderItem.desc(orderField));
            }
        }

        //没有排序字段，则不排序
        if (StringUtils.isBlank(defaultOrderField)) {
            return page;
        }

        //默认排序
        if (isAsc) {
            page.addOrder(OrderItem.asc(defaultOrderField));
        } else {
            page.addOrder(OrderItem.desc(defaultOrderField));
        }

        return page;
    }
}
