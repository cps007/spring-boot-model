//package cn.springboot.model.dao.config;
//
//import com.alibaba.druid.filter.Filter;
//import com.alibaba.druid.filter.stat.StatFilter;
//import com.alibaba.druid.pool.DruidDataSource;
//import com.alibaba.druid.support.http.StatViewServlet;
//import com.google.common.collect.Lists;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.web.servlet.ServletRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//import java.sql.SQLException;
//
///**
// * @Author jf
// * @Description druid源配置
// * @Date 2020/8/31 15:58
// * @Version 1.0
// */
//@Configuration
//public class DruidConfig {
//    @ConfigurationProperties(prefix = "spring.druid")
//    @Bean(initMethod = "init", destroyMethod = "close")
//    public DataSource dataSource(Filter statFilter) throws SQLException {
//        DruidDataSource dataSource = new DruidDataSource();
//        dataSource.setProxyFilters(Lists.newArrayList(statFilter()));
//        return dataSource;
//    }
//
//    @Bean
//    public Filter statFilter() {
//        StatFilter filter = new StatFilter();
//        filter.setSlowSqlMillis(5000);
//        filter.setLogSlowSql(true);
//        filter.setMergeSql(true);
//        return filter;
//    }
//
//
//    @Bean
//    public ServletRegistrationBean servletRegistrationBean() {
//        return new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
//    }
//
//}
