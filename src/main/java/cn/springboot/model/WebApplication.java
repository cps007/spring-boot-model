package cn.springboot.model;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author jf
 */
@SpringBootApplication(scanBasePackages = "cn.springboot.model")
public class WebApplication {

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(WebApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        port = port == null ? "8080" : port;
        String path = env.getProperty("spring.mvc.servlet.path");
        path = path == null ? "" : path.length() > 1 ? path + "/" : "/";
        //swagger-ui.html、swagger-ui/index.html、doc.html、
        System.out.println("\n========================================================================\n\t" +
                "Application Demo is running! Access URLs:\n\t" +
                "本地访问地址: \thttp://localhost:" + port + path + "\n\t" +
                "外部访问地址: \thttp://" + ip + ":" + port + path + "\n\t" +
                "Swagger 文档: \thttp://" + ip + ":" + port + path + "doc.html\n\t" +
                "========================================================================");
    }
}
