package cn.springboot.model.service;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;

/**
 * @author jf
 * @version 1.0
 * @Description mybatisplus 代码生成器（新）
 * @date 2022/06/06 19:28
 */
public class MysqlGenerator {
    public static void main(String[] args) {
        FastAutoGenerator.create(new DataSourceConfig.Builder("jdbc:mysql://localhost:3306/model",
                        "root", "root").schema("model"))
                .globalConfig(builder -> {
                    builder.author("jf")
                            .outputDir("E://code//idea//spring-boot-model//spring-boot-model-service//src//main//java//cn//springboot//model//service//generator");
                })
                .packageConfig(builder -> builder.parent("cn.springboot.model.service"))
                .execute();
    }
}
