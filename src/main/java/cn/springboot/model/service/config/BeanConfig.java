package cn.springboot.model.service.config;

import cn.springboot.model.service.DemoClassService;
import cn.springboot.model.service.DemoStuService;
import cn.springboot.model.service.impl.DemoClassServiceImpl;
import cn.springboot.model.service.impl.DemoStuServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jf
 */
@Configuration
public class BeanConfig {

    @Bean
    public DemoClassService demoClassService() {
        return new DemoClassServiceImpl();
    }

    @Bean
    public DemoStuService demoStuService() {
        return new DemoStuServiceImpl();
    }
}