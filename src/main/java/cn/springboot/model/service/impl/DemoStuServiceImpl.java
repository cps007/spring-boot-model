package cn.springboot.model.service.impl;

import cn.springboot.model.base.common.R;
import cn.springboot.model.base.utils.StringUtils;
import cn.springboot.model.dao.entity.DemoStuEntity;
import cn.springboot.model.dao.mapper.DemoStuMapper;
import cn.springboot.model.dao.utils.Pages;
import cn.springboot.model.dao.utils.QueryPage;
import cn.springboot.model.service.DemoStuService;
import cn.springboot.model.service.param.QueryPageParam;
import cn.springboot.model.service.utils.RedisUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 服务接口实现
 *
 * @author jf
 * @description
 * @since 2022-05-31 21:44:29
 */
@Service
public class DemoStuServiceImpl extends ServiceImpl<DemoStuMapper, DemoStuEntity> implements DemoStuService {

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 自动删除学生
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public R deleStu() {
        LambdaQueryWrapper<DemoStuEntity> lambdaQuery = Wrappers.<DemoStuEntity>lambdaQuery();
        lambdaQuery.ge(DemoStuEntity::getId, 1).or().ge(DemoStuEntity::getStuAge, 0);
        baseMapper.delete(lambdaQuery);
        Set<String> keys = redisUtils.common.keys("getStu:current:*");
        redisUtils.common.delete(keys);
        return R.ok("删除🆗").put("keys", keys);
    }


    /**
     * 自动添加学生
     * <p>
     * 每个班级都有学生1-70位
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public R addStu() {
        int index = 1;
        int stus = 54;
        int clas = 41;
        DemoStuEntity demoStuEntity = null;
        List<DemoStuEntity> stuList = new ArrayList<>();
        for (int stu = 1; stu <= stus; stu++) {
            for (int claId = 1; claId <= clas; claId++) {
                demoStuEntity = new DemoStuEntity();
                demoStuEntity.setStuName("小山" + stu);
                demoStuEntity.setStuAge("" + autoAge());
                demoStuEntity.setId((long) index);
                demoStuEntity.setClassId((long) claId);
                stuList.add(index - 1, demoStuEntity);
                index++;
            }
        }
        saveBatch(stuList);
        IPage<DemoStuEntity> selectPage = baseMapper.selectPage(new QueryPage<DemoStuEntity>().getPage(1, 25), new QueryWrapper<>());
        Pages pages = new Pages(selectPage);
        redisUtils.value.set("getStu:current:" + selectPage.getCurrent(), selectPage.getTotal() > 0 ? pages : null, redisUtils.autoSeconds() * 2);
        return R.ok("添加🆗");
    }

    private int autoAge() {
        int min = 16;
        int max = 26;
        return (int) ((Math.random() * (max - min)) + min);
    }

    @Override
    public R getStuById(Integer id) {
        return R.ok().put("data", baseMapper.selectById(id));
    }

    @Override
    public R getStu(QueryPageParam queryPage) {

        IPage<DemoStuEntity> data = null;
        Pages getStuData = redisUtils.value.get("getStu:current:" + queryPage.getCurrent());
        if (StringUtils.isNull(getStuData)) {
            // 无缓存
            data = baseMapper.selectPage(new QueryPage<DemoStuEntity>().getPage(queryPage.getCurrent(), queryPage.getLimit()), new QueryWrapper<>());
            Pages pages = new Pages(data);
            redisUtils.value.set("getStu:current:" + queryPage.getCurrent(), data.getTotal() > 0 ? pages : null, redisUtils.autoSeconds() * 2);
            return R.ok().put("data", pages);
        }
        // 有缓存
        if (getStuData == null || getStuData.equals("null")) {
            return R.ok().put("data", new Pages());
        }
//        Pages resultData = JSON.parseObject(getStuData, new TypeReference<Pages>() {
//        });
        //更新时间
        redisUtils.common.expire("getStu:current:" + queryPage.getCurrent(), redisUtils.autoSeconds() * 2);
        return R.ok().put("data", getStuData);
    }
}