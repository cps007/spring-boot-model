package cn.springboot.model.service.impl;

import cn.springboot.model.base.common.R;
import cn.springboot.model.base.exception.GlobalException;
import cn.springboot.model.base.utils.StringUtils;
import cn.springboot.model.dao.entity.DemoClassEntity;
import cn.springboot.model.dao.mapper.DemoClassMapper;
import cn.springboot.model.dao.utils.Pages;
import cn.springboot.model.dao.utils.QueryPage;
import cn.springboot.model.service.DemoClassService;
import cn.springboot.model.service.param.QueryPageParam;
import cn.springboot.model.service.utils.RedisUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 服务接口实现
 *
 * @author jf
 * @description
 * @since 2022-05-31 21:44:29
 */
@Service
public class DemoClassServiceImpl extends ServiceImpl<DemoClassMapper, DemoClassEntity> implements DemoClassService {
    @Autowired
    private RedisUtils redisUtils;

    /**
     * 自动删除班级
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public R deleClas() {
        LambdaQueryWrapper<DemoClassEntity> lambdaQuery = Wrappers.<DemoClassEntity>lambdaQuery();
        lambdaQuery.ge(DemoClassEntity::getId, 1);
        baseMapper.delete(lambdaQuery);
        Set<String> keys = redisUtils.common.keys("getClas:current:*");
        redisUtils.common.delete(keys);
        return R.ok("删除🆗").put("keys", keys);
    }

    /**
     * 自动添加班级（10*4=40）
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public R addClas() throws GlobalException {
        int index = 1;
        DemoClassEntity demoClassEntity = null;
        List<DemoClassEntity> stuList = new ArrayList<>();
        for (int nj = 1; nj < 5; nj++) {
            for (int bj = 1; bj < 11; bj++) {
                demoClassEntity = new DemoClassEntity();
                demoClassEntity.setId((long) index);
                demoClassEntity.setClassName(getCNJ(nj));
                demoClassEntity.setClassMn(bj + "班");
                stuList.add(index - 1, demoClassEntity);
                index++;
            }
        }
        saveBatch(stuList);
        IPage<DemoClassEntity> selectPage = baseMapper.selectPage(new QueryPage<DemoClassEntity>().getPage(1, 25), new QueryWrapper<>());
        Pages pages = new Pages(selectPage);
        redisUtils.value.set("getClas:current:" + selectPage.getCurrent(), selectPage.getTotal() > 0 ? pages : null, redisUtils.autoSeconds() * 2);
        return R.ok("添加🆗");
    }

    /**
     * 返回 大一 ...
     *
     * @param nj
     * @return
     */
    private String getCNJ(Integer nj) {
        return switch (nj) {
            case 1 -> "大一";
            case 2 -> "大二";
            case 3 -> "大三";
            case 4 -> "大四";
            default -> "";
        };
    }

    @Override
    public R getClassById(Integer id) {
        return R.ok().put("data", baseMapper.selectById(id));
    }

    @Override
    public R getClas(QueryPageParam queryPage) {

        IPage<DemoClassEntity> data = null;
        Pages getStuData = redisUtils.value.get("getClas:current:" + queryPage.getCurrent());
        if (StringUtils.isNull(getStuData)) {
            // 无缓存
            data = baseMapper.selectPage(new QueryPage<DemoClassEntity>().getPage(queryPage.getCurrent(), queryPage.getLimit()), new QueryWrapper<>());
            Pages pages = new Pages(data);
            redisUtils.value.set("getClas:current:" + queryPage.getCurrent(), data.getTotal() > 0 ? pages : null, redisUtils.autoSeconds() * 2);
            return R.ok().put("data", pages);
        }
        // 有缓存
        if (getStuData == null || getStuData.equals("null")) {
            return R.ok().put("data", new Pages());
        }
        redisUtils.common.expire("getClas:current:" + queryPage.getCurrent(), redisUtils.autoSeconds() * 2);
        return R.ok().put("data", getStuData);
    }
}