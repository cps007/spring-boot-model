package cn.springboot.model.service.impl;

import cn.springboot.model.dao.entity.SysLogEntity;
import cn.springboot.model.dao.mapper.SysLogMapper;
import cn.springboot.model.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author jf
 * @description 针对表【sys_log(操作日志记录)】的数据库操作Service实现
 * @createDate 2022-06-02 12:43:04
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements SysLogService {

}




