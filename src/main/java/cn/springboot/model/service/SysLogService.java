package cn.springboot.model.service;

import cn.springboot.model.dao.entity.SysLogEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author jf
 * @description 针对表【sys_log(操作日志记录)】的数据库操作Service
 * @createDate 2022-06-02 12:43:04
 */
public interface SysLogService extends IService<SysLogEntity> {

}
