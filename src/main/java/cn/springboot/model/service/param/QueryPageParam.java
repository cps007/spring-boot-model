package cn.springboot.model.service.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jf
 * @version 1.0
 * @Description 分页参数查询
 * @date 2022/06/03 14:48
 */
@Data
@ApiModel(value = "分页查询参数", description = "分页查询参数")
public class QueryPageParam {
    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页", required = true)
    private long current = 1;

    /**
     * 每页显示条数，默认 10
     */
    @ApiModelProperty(value = "每页显示条数,默认10", required = false)
    private long limit = 10;

}
