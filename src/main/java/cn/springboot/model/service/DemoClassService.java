package cn.springboot.model.service;


import cn.springboot.model.base.common.R;
import cn.springboot.model.dao.entity.DemoClassEntity;
import cn.springboot.model.service.param.QueryPageParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务接口
 *
 * @author jf
 * @description
 * @since 2022-05-31 21:44:29
 */
public interface DemoClassService extends IService<DemoClassEntity> {
    /**
     * 自动删除班级
     */
    public R deleClas();

    /**
     * 自动添加班级
     */
    public R addClas();

    R getClassById(Integer id);

    R getClas(QueryPageParam queryPage);
}
