package cn.springboot.model.service;


import cn.springboot.model.base.common.R;
import cn.springboot.model.dao.entity.DemoStuEntity;
import cn.springboot.model.service.param.QueryPageParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 服务接口
 *
 * @author jf
 * @description
 * @since 2022-05-31 21:44:29
 */
public interface DemoStuService extends IService<DemoStuEntity> {
    /**
     * 自动添加学生
     */
    public R deleStu();

    /**
     * 自动添加学生
     */
    public R addStu();

    R getStuById(Integer id);

    R getStu(QueryPageParam queryPage);

}
