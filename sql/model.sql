/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 5.7.38 : Database - model
*********************************************************************
*/
CREATE DATABASE model;

USE model;

DROP TABLE IF EXISTS demo_class;

CREATE TABLE demo_class
(
    id         bigint(20)  NOT NULL COMMENT 'id',
    class_name varchar(20) NOT NULL COMMENT '班级名称',
    class_mn   varchar(20) NOT NULL COMMENT '班级编号',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;


DROP TABLE IF EXISTS demo_stu;

CREATE TABLE demo_stu
(
    id       bigint(20)  NOT NULL COMMENT 'id',
    stu_name varchar(20) NOT NULL COMMENT '学生名称',
    stu_age  varchar(20) NOT NULL COMMENT '年龄',
    class_id bigint(20)  NOT NULL COMMENT '班级编号',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;


DROP TABLE IF EXISTS sys_log;

CREATE TABLE sys_log
(
    id             bigint(20)    NOT NULL AUTO_INCREMENT COMMENT '日志主键',
    user_name      varchar(20)   NOT NULL DEFAULT 'null' COMMENT '用户',
    operation      varchar(50)   NOT NULL DEFAULT 'null' COMMENT '操作详情',
    url            varchar(255)  NOT NULL DEFAULT 'null' COMMENT '请求URL',
    method         varchar(100)  NOT NULL DEFAULT 'null' COMMENT '请求方法',
    request_method varchar(10)   NOT NULL DEFAULT 'null' COMMENT '请求方式',
    params         varchar(200)  NOT NULL DEFAULT 'null' COMMENT '请求参数',
    run_time       bigint(20)    NOT NULL DEFAULT '0' COMMENT '执行耗时',
    ip             varchar(150)  NOT NULL DEFAULT 'null' COMMENT '用户ip',
    location       varchar(20)   NOT NULL DEFAULT 'null' COMMENT '用户省市名称',
    title          varchar(50)   NOT NULL DEFAULT 'null' COMMENT '模块标题',
    business_type  int(2)        NOT NULL DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
    type           int(1)        NOT NULL DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
    json_result    varchar(2000) NOT NULL DEFAULT 'null' COMMENT '返回参数',
    status         int(1)        NOT NULL DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
    error_msg      varchar(2000) NOT NULL DEFAULT 'null' COMMENT '错误消息',
    create_time    datetime      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户请求操作时间',
    PRIMARY KEY (id)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4 COMMENT ='操作日志记录';
