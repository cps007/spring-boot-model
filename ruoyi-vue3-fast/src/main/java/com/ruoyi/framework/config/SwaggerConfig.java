package com.ruoyi.framework.config;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * Swagger2的接口配置
 *
 * @author ruoyi
 */
@Configuration
public class SwaggerConfig {
    /**
     * 系统基础配置
     */
    @Autowired
    private RuoYiConfig ruoyiConfig;

    /**
     * 是否开启swagger
     */
    @Value("${swagger.enabled}")
    private boolean enabled;

    /**
     * 设置请求的统一前缀
     */
    @Value("${swagger.pathMapping}")
    private String pathMapping;


    /**
     * 分组:sys管理
     *
     * @return Docket
     */
    @Bean
    public Docket sys_api_app() {
        return new Docket(DocumentationType.OAS_30)
                // 是否启用Swagger
                .enable(enabled)
                // 用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .apiInfo(apiInfo("标题：系统管理模块_API", "system"))
                // 设置哪些接口暴露给Swagger展示
                .select()
/////////////////////////////////////////////////////扫描
                .apis(RequestHandlerSelectors.basePackage("com.ruoyi.project.system.controller"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // .apis(RequestHandlerSelectors.any())
/////////////////////////////////////////////////////请求路径
                //.paths(PathSelectors.any())
                .paths(PathSelectors.ant("/system/**"))
                .build()
/////////////////////////////////////////////////////设置安全模式，swagger可以设置访问token
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts())
                .groupName("system")
                .pathMapping(pathMapping);
    }

    /**
     * 分组: monitor 系统监视器
     *
     * @return Docket
     */
    @Bean
    public Docket monitor_api_app() {
        return new Docket(DocumentationType.OAS_30)
                // 是否启用Swagger
                .enable(enabled)
                // 用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .apiInfo(apiInfo("标题：系统监视模块_API", "monitor"))
                // 设置哪些接口暴露给Swagger展示
                .select()
/////////////////////////////////////////////////////扫描
                .apis(RequestHandlerSelectors.basePackage("com.ruoyi.project.monitor.controller"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // .apis(RequestHandlerSelectors.any())
/////////////////////////////////////////////////////请求路径
                //.paths(PathSelectors.any())
                .paths(PathSelectors.ant("/monitor/**"))
                .build()
/////////////////////////////////////////////////////设置安全模式，swagger可以设置访问token
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts())
                .groupName("monitor")
                .pathMapping(pathMapping + "/monitor");
    }

    /**
     * 分组: tool 管理
     *
     * @return Docket
     */
    @Bean
    public Docket tool_api_app() {
        return new Docket(DocumentationType.OAS_30)
                // 是否启用Swagger
                .enable(enabled)
                // 用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .apiInfo(apiInfo("标题：系统工具模块_API", "tool"))
                // 哪些接口暴露给Swagger展示
                .select()
/////////////////////////////////////////////////////扫描
                .apis(RequestHandlerSelectors.basePackage("com.ruoyi.project.tool.gen.controller"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // .apis(RequestHandlerSelectors.any())
/////////////////////////////////////////////////////请求路径
                //.paths(PathSelectors.any())
                .paths(PathSelectors.ant("/tool/**"))
                .build()
/////////////////////////////////////////////////////设置安全模式，swagger可以设置访问token
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts())
                .groupName("tool")
                .pathMapping(pathMapping + "/tool");
    }

    @Bean
    public Docket test_api_app() {
        return new Docket(DocumentationType.OAS_30)
                // 是否启用Swagger
                .enable(enabled)
                .apiInfo(apiInfo("标题：swagger 测试模块_API", "test"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ruoyi.project.tool.swagger"))
                //.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // .apis(RequestHandlerSelectors.any())
                //.paths(PathSelectors.any())
                .paths(PathSelectors.ant("/test/**"))
                .build()
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts())
                .groupName("test")
                .pathMapping(pathMapping + "/test");
    }

    /**
     * 安全模式，这里指定token通过Authorization头请求头传递
     */
    private List<SecurityScheme> securitySchemes() {
        List<SecurityScheme> apiKeyList = new ArrayList<SecurityScheme>();
        apiKeyList.add(new ApiKey("Authorization", "Authorization", In.HEADER.toValue()));
        return apiKeyList;
    }

    /**
     * 安全上下文
     */
    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts = new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .operationSelector(o -> o.requestMappingPattern().matches("/.*"))
                        .build());
        return securityContexts;
    }

    /**
     * 默认的安全上引用
     */
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference("Authorization", authorizationScopes));
        return securityReferences;
    }

    /**
     * 构建api文档的详细信息
     *
     * @param title       标题
     * @param description 描述
     * @return ApiInfo
     */
    private ApiInfo apiInfo(String title, String description) {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                // 作者信息
                .contact(new Contact(ruoyiConfig.getName(), null, null))
                .version("版本号:" + ruoyiConfig.getVersion())
                .build();
    }
}
